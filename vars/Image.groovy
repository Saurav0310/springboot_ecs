import groovy.json.JsonBuilder  
import groovy.json.JsonSlurper
import groovy.transform.Field 
def build() {
    script {
        sh """#!/bin/bash
                   docker build -t ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${BUILD_NUMBER} .
                   docker tag ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${BUILD_NUMBER} ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-latest
                   aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${DOCKER_REGISTRY} 
        """
    }
}
