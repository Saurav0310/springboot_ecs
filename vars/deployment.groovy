def call(def pipelineParams){
pipeline {
    agent any
    parameters {
      choice(
        name: 'DOCKER_SERVICE',
        description: 'Which tests to run?',
        choices: ['post','read','client']
            )
    }
    
    environment {
        ACCOUNT_NO = "991037556739"
        Deployment_git_url = "${pipelineParams.Deployment_git_url}"
        APP_NAME = "${pipelineParams.APP_NAME}"
        REPO_TO_ADD = "https://gitlab.com/Saurav0310/${APP_NAME}.git"
        branch = "${pipelineParams.branch}"
        REGION = "${pipelineParams.REGION}"
        //GIT_COMMIT_REV = "${pipelineParams.GIT_COMMIT_REV}"
        ENV = "${pipelineParams.ENV}"
        DOCKER_SERVICE = "${pipelineParams.DOCKER_SERVICE}"
        DOCKER_REGISTRY = "${ACCOUNT_NO}.dkr.ecr.${REGION}.amazonaws.com"
    }
    stages {
        stage('Clean workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Checkout Code') {
            steps {
                checkout([
                $class: 'GitSCM',
                branches: [[name:  branch ]],
                userRemoteConfigs: [[ url: REPO_TO_ADD ]]
                ])
            }   
        }
        /*stage('Sonarqube Setup') {
            steps {
                script {
                sh '''#!/bin/bash
                    /opt/sonar-scanner-4.8.0.2856-linux/bin/sonar-scanner -X \
                    -Dproject.settings=sonar-project.properties \
                    -Dsonar.host.url=http://sonarqube.tothenew.net \
                    -Dsonar.login=af351098c60c71f3cc3f9f13abb8e976b2316ce2
                  '''
                }
            }
        }*/
        /*stage('Jenkinsfile adding ') {
            steps {
              script{
              utility.createJenkinsfile()
              }
              sh"""
              sed -i 's@REPO_TO_ADD@'"${REPO_TO_ADD}" Jenkinsfile
              cat Jenkinsfile
              """
            }
          }*/         
        stage('Configure') {
            steps {
              script {    
                env.GIT_COMMIT_REV = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
              }
            }
        }
        stage('Print Variables'){
            steps {
                sh """
                echo $REPO_TO_ADD
                echo $branch
                echo $GIT_COMMIT_REV
                echo $REPO_TO_ADD
                echo $APP_NAME
                """
            }
        }
        stage('Testing') {
            steps{
              script{
                utility.helmInstall("Testing")
              }
            }
        }
        stage('Everthing') {
            when { anyOf { environment name: 'ENV', value:'prod'; environment name: 'ENV', value: 'qa'} }        
            steps {
              script{
                Image.build()
              }
            }
        }
        stage('Checkout Code for deployment') {
            steps {
                checkout([
                $class: 'GitSCM',
                branches: [[name:  branch ]],
                userRemoteConfigs: [[ url: Deployment_git_url ]]
                ])
            }   
        }
    }
  }
}
