def call(Map pipelineArgs){
        node('master') {
            stage('Cleaning Up Workspace'){
                utility.cleanWorkspace()   
            }
        }

        stage('Checkout repository') {
            node('master') {
            /**
            *Initializing Name Configuration for Job
            **/
            //env.ORG = "${params.REPO_TO_ADD}".split(':')[1].split('/.')[0]
            //env.APP_NAME = "${params.REPO_TO_ADD}".split('/')[1].split('\\.')[0]
            //if("${params.REPO_TO_ADD}".split('/')[1].contains('@')){
            //    env.REPO_URL = "${params.REPO_TO_ADD}".split(':')[2].split('@')[1]
            //}
            //else{
            //    env.REPO_URL = "${params.REPO_TO_ADD}".split('@')[1].split(':')[0]
            //}
            env.ORG = "${params.REPO_TO_ADD}".split(':')[1].split('/.')[0]
            env.APP_NAME = "${params.REPO_TO_ADD}".split('/')[1].split('\\.')[0]
            if("${params.REPO_TO_ADD}".split('/')[1].contains('@')){
                env.REPO_URL = "${params.REPO_TO_ADD}".split(':')[2].split('@')[1]
            }
            else{
                env.REPO_URL = "${params.REPO_TO_ADD}".split('@')[1].split(':')[0]
            }
            env.GIT_CRED_ID = 'saurav'
            env.JKS_CRED_ID = 'saurav'
            echo "ORG: ${ORG}"
            echo "REPO_URL: ${REPO_URL}"
            echo "REPO_TO_ADD: ${params.REPO_TO_ADD}"
            echo "APP_NAME: ${APP_NAME}"
            utility.cloneRepo()
            checkout([$class: 'GitSCM', branches: [[name: "${BRANCH_TO_CONFIGURE}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: false, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: "GitSSH", url: "git@${REPO_URL}:${ORG}/${APP_NAME}.git"]]])
        }
        }

        stage('Create Jenkinsfile and Dockerfile'){
            node('master') {
            utility.createDockerfile()
            utility.createJenkinsfile()
            utility.prepareFiles()
            }
        }
        stage('Commit the files to repo') {
            node('master') {
                sh """
                    git checkout ${BRANCH_TO_CONFIGURE}
                    git add . && git commit -a -m \"Added Jenkinsfile and Dockerfile\" || :
                    git push origin ${BRANCH_TO_CONFIGURE}
                """
            }
        }
        stage('Create ECR for service') {
            node('master') {
                sh """
                    aws ecr create-repository --repository-name nw-social-${APP_NAME}-qa &> /dev/null
                """
            }
        }
        stage('Create a job in Jenkins') {
            node('master') {
                withCredentials([usernamePassword(credentialsId: 'Jenkinspat', passwordVariable: 'JKS_PASSWORD', usernameVariable: 'JKS_USERNAME')])  {
                    sh """
                        sed -i 's/replace_the_repository/${APP_NAME}/g' template.xml
                        sed -ie "s+git_repo+"${params.REPO_TO_ADD}"+g" template.xml
                        curl -s -X POST 'http://localhost:8080/createItem?name=${APP_NAME}' -u ${JKS_USERNAME}:${JKS_PASSWORD} --data-binary @template.xml -H "Content-Type:text/xml"
                        sleep 20
                        cat template.xml
                        echo "Follow below URL: http://localhost:8080/job/NW-services/job/${APP_NAME}/"
                    """
                }
            }
        }
}
