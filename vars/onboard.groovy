def call(def pipelineArgs){
pipeline {
    agent any
    parameters {
      choice(
        name: 'DOCKER_SERVICE',
        description: 'Which tests to run?',
        choices: ['post','read','client',]
            )
    }
    
    environment {
        ACCOUNT_NO = "${pipelineParams.ACCOUNT_NO}"
        url = "${pipelineParams.url}"
        branch = "${pipelineParams.branch}"
        REGION = "${pipelineParams.REGION}"
        GIT_COMMIT_REV = "${pipelineParams.GIT_COMMIT_REV}"
        ENV = "${pipelineParams.ENV}"
        DOCKER_SERVICE = "${pipelineParams.DOCKER_SERVICE}"
        DOCKER_REGISTRY = "${pipelineParams.ACCOUNT_NO}.dkr.ecr.${REGION}.amazonaws.com"
    }
    stages {
        stage('Clean workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Checkout Code') {
            steps {
                checkout([
                $class: 'GitSCM',
                branches: [[name:  branch ]],
                userRemoteConfigs: [[ url: url ]]
                ])
            }   
        }
        stage('Create Jenkinsfile and Dockerfile'){
            steps{
            utility.createDockerfile()
            utility.createJenkinsfile()
            }
        }
    }
}
}
