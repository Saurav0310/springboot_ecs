def call(def pipelineParams){
pipeline {
    agent any
    environment {
        ACCOUNT_NO = "${pipelineParams.ACCOUNT_NO}"
        url = "${pipelineParams.url}"
        branch = "${pipelineParams.branch}"
        REGION = "${pipelineParams.REGION}"
        ENV = "${pipelineParams.ENV}"
        DOCKER_SERVICE = "${pipelineParams.DOCKER_SERVICE}"
        DOCKER_REGISTRY = "${pipelineParams.ACCOUNT_NO}.dkr.ecr.${REGION}.amazonaws.com"
    }
    stages {
        stage('Clean workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Print Variables'){
            steps {
                sh """
                echo $url
                echo $branch
                """
            }
        }
        stage('Checkout Code') {
            steps {
                checkout([
                $class: 'GitSCM',
                branches: [[name:  branch ]],
                userRemoteConfigs: [[ url: url ]]
                ])
            }   
        }
        stage('Build Image') {
           steps{
                   sh """
                   docker build -t ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER} .
                   docker tag ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER} ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-latest
                   """
                }
            }
        stage('Login to ECR  image and Push image') {
            steps {
                sh """
                   aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${DOCKER_REGISTRY} 
                   docker tag ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER} ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-latestme AWS --password-stdin ${DOCKER_REGISTRY}
                   docker push ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER}
                   docker push ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-latest
                   docker rmi -f ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER}
                   docker rmi -f ${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-latest
                   rm -r /var/lib/jenkins/.docker/config.json
                   """
               }
            }
                           
        stage('kubernetes deploy') {
           steps{
                sh """
                kubectl config set-context --namespace=default --current
                kubectl set image deployment/nw-social-${DOCKER_SERVICE}-${ENV} nw-social-${DOCKER_SERVICE}-${ENV}=${DOCKER_REGISTRY}/nw-social-${DOCKER_SERVICE}:${ENV}-${env.BUILD_NUMBER}'
                kubectl rollout status deployment.v1.apps/nw-social-${DOCKER_SERVICE}-${ENV}
                """
            }
        } 
    }
        
    }
    post 
    {
        always 
        {   
            googlechatnotification message: 'Build_check for Job ${JOB_NAME} with ${BUILD_URL} is ${BUILD_STATUS} by ${BUILD_USER}', 
            notifyAborted: true, 
            notifyBackToNormal: true, 
            notifyFailure: true, 
            notifyNotBuilt: true, 
            notifySuccess: true, 
            notifyUnstable: true, 
            suppressInfoLoggers: true, 
            url: ''
        }
    }
}



