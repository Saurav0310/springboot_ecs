#!/usr/bin/env groovy

/**
 * Send notifications based on build status string
 */
def call(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus = buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#BD3855'
  def subject = "${buildStatus}-Job :${env.JOB_NAME}\nGIT_COMMIT_ID :${env.GIT_COMMIT_REV}\nBuild-no:[${env.BUILD_NUMBER}]"
  def summary = "${subject}\nBUILD_URL:${env.BUILD_URL}"
  def details = """<p>${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  // Override default values based on build status
  if (buildStatus == 'ABORTED') {
    color = 'YELLOW'
    colorCode = '#DAAD4F'
  } else if (buildStatus == 'SUCCESS') {
    color = 'GREEN'
    colorCode = '#5CB589'
    summary = summary 
  } else {
    color = 'RED'
    colorCode = '#BD3855'
  }

  // Send notifications
  googlechatnotification (color: colorCode, notify: true, message: summary)

}

